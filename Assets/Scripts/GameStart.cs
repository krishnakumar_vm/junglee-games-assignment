﻿using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace com.junglee.cards
{
    // Game start
    public class GameStart : MonoBehaviour
    {
        public Button m_StartButton;

        void Start()
        {
            m_StartButton.OnClickAsObservable().Subscribe(_ => OnSratButtonClick());
        }

        private void OnSratButtonClick()
        {
            SceneManager.LoadScene("GameScene");
        }
    }
}