﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace com.junglee.cards
{
    /// <summary>
    /// Creating cards according to json and handles it
    /// </summary>
    public class CardManager : MonoBehaviour
    {
        [Inject]
        private CardWidget.Factory _CardWidgetFactory;
        [Inject]
        GameDataHandler _GameDataHandler;

        [SerializeField]
        private Transform m_WidgetPatent;
        [SerializeField]
        private Button m_GroupButton;
        [SerializeField]
        private Button m_ResetGroupButton;
        [SerializeField]
        private Transform m_GroupParent;

        public CardsDeck m_CardsDeck;
        bool IsCardCreated;

        void Start()
        {
            IsCardCreated = false;
            if (_GameDataHandler.IsGameDataLoaded.Value)
                OnGameDataLoaded();

            m_GroupButton.gameObject.SetActive(false);
            m_GroupButton.interactable = false;
            m_ResetGroupButton.gameObject.SetActive(false);
            m_ResetGroupButton.interactable = false;

            SubscribeEvents();
        }

        //Subscribe Events and listners 
        private void SubscribeEvents()
        {
            _GameDataHandler.IsGameDataLoaded.AsObservable().Subscribe(x => OnGameDataLoadedChange(x));

            _GameDataHandler.ResetButtonState += OnResetButtonState;
            _GameDataHandler.OnWidgetAdded += OnWidgetAdded;

            m_GroupButton.onClick.AddListener(OnGroupButtonClick);
            m_ResetGroupButton.onClick.AddListener(OnResetGroupButtonClick);
        }

        //on rest  grouped items
        private void OnResetGroupButtonClick()
        {
            for (int i = 0; i < _GameDataHandler.m_CurrentWidgets.Count; i++)
            {
                _GameDataHandler.m_CurrentWidgets[i].GetComponent<CanvasGroup>().blocksRaycasts = true;
                _GameDataHandler.m_CurrentWidgets[i].transform.SetParent(m_WidgetPatent);
            }
            _GameDataHandler.m_CurrentWidgets.Clear();
            _GameDataHandler.ResetButtonState();
            m_ResetGroupButton.gameObject.SetActive(false);
            m_ResetGroupButton.interactable = false;
        }

        private void OnGameDataLoaded()
        {
            IsCardCreated = false;
            CreateCards();
        }

        //checking for card deck is created
        private void OnGameDataLoadedChange(bool val)
        {
            if (val && IsCardCreated)
            {
                OnGameDataLoaded();
            }
        }

        //checking if multiple cards are selected or not
        private void OnWidgetAdded()
        {
            if (_GameDataHandler.m_WidgetsList.Count > 1)
            {
                m_GroupButton.gameObject.SetActive(true);
                m_GroupButton.interactable = true;
            }
            else
            {
                m_GroupButton.gameObject.SetActive(false);
                m_GroupButton.interactable = false;
            }
        }

        private void OnDestroy()
        {
            m_GroupButton.onClick.RemoveListener(OnGroupButtonClick);
            m_ResetGroupButton.onClick.RemoveListener(OnResetGroupButtonClick);

            _GameDataHandler.ResetButtonState -= OnResetButtonState;
            _GameDataHandler.OnWidgetAdded -= OnWidgetAdded;
        }

        //create group items
        private void OnGroupButtonClick()
        {
            for (int i = 0; i < _GameDataHandler.m_WidgetsList.Count; i++)
            {
                _GameDataHandler.m_WidgetsList[i].GetComponent<CanvasGroup>().blocksRaycasts = false;
                _GameDataHandler.m_WidgetsList[i].transform.SetParent(m_GroupParent);

                _GameDataHandler.m_CurrentWidgets.Add(_GameDataHandler.m_WidgetsList[i]);

            }
            _GameDataHandler.ResetButtonState();
            m_ResetGroupButton.gameObject.SetActive(true);
            m_ResetGroupButton.interactable = true;
        }

        //reset selected cards
        private void OnResetButtonState()
        {
            _GameDataHandler.m_WidgetsList.Clear();
            m_GroupButton.gameObject.SetActive(false);
            m_GroupButton.interactable = false;
        }

        //Creates cards according to json data
        List<string> _cardsList;
        private void CreateCards()
        {
            _cardsList = _GameDataHandler.m_CardsData.value.cards;
            if (_cardsList.Count > 0)
            {
                for (int i = 0; i < _cardsList.Count; i++)
                {
                    GenerateCardItems(_cardsList[i]);
                }
            }
            else
            {
                Debug.Log("No data found!!");
            }
        }

        //Generate item with factories
        private void GenerateCardItems(string card)
        {
            CardWidget _widget = _CardWidgetFactory.Create();

            if (m_CardsDeck.m_CarsDict[card])
            {
                _widget.transform.SetParent(m_WidgetPatent);
                _widget.GetComponentInChildren<Image>().sprite = m_CardsDeck.m_CarsDict[card];
            }
        }
    }
}
