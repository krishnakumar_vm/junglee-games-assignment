﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Zenject;
using UniRx;

namespace com.junglee.cards
{
    public class CardWidget : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public class Factory : PlaceholderFactory<CardWidget> { }

        [Inject]
        GameDataHandler _GameDataHandler;

        #region CardSelection Fields
        [SerializeField]
        private Button _WidgetButton;
        [SerializeField]
        private GameObject _WidgetSelectIndicator;
        #endregion

        #region Drag Fields
        [HideInInspector]
        public Transform parentToReturnTo = null;
        [HideInInspector]
        public Transform placeholderParent = null;
        GameObject placeholder = null;
        private CanvasGroup _canvasGroup;
        #endregion Drag Fields

        #region CardSelection

        private void Start()
        {
            _canvasGroup = GetComponent<CanvasGroup>();

            if (null != _GameDataHandler)
                _GameDataHandler.ResetButtonState += OnResetButtonState;
            _WidgetSelectIndicator.SetActive(false);

            _WidgetButton.OnClickAsObservable().Subscribe(_ => OnWidgetButtonClick());
        }

        //Reset grroped objects
        private void OnResetButtonState()
        {
            _WidgetSelectIndicator.SetActive(false);
        }

        //selct card item
        private void OnWidgetButtonClick()
        {
            _WidgetSelectIndicator.SetActive(true);

            if (null != _GameDataHandler)
                _GameDataHandler.AddClickedWidget(this.gameObject);
        }

        private void OnDestroy()
        {
            if (null != _GameDataHandler)
                _GameDataHandler.ResetButtonState -= OnResetButtonState;
        }

        #endregion

        #region Drag Functions
        /// <summary>
        /// Here tracking card drag and place it on dag end
        /// handling placeholder and its position
        /// </summary>

        //on draging card
        public void OnDrag(PointerEventData eventData)
        {
            this.transform.position = eventData.position;

            if (placeholder.transform.parent != placeholderParent)
                placeholder.transform.SetParent(placeholderParent);

            int newSiblingIndex = placeholderParent.childCount;
            for (int i = 0; i < placeholderParent.childCount; i++)
            {
                if (this.transform.position.x < placeholderParent.GetChild(i).position.x)
                {
                    newSiblingIndex = i;
                    if (placeholder.transform.GetSiblingIndex() < newSiblingIndex)
                        newSiblingIndex--;
                    break;
                }
            }
            placeholder.transform.SetSiblingIndex(newSiblingIndex);
        }

        //On start drag the card
        public void OnBeginDrag(PointerEventData eventData)
        {
            if (null != _GameDataHandler)
                _GameDataHandler.ResetButtonState();
            placeholder = new GameObject();
            placeholder.transform.SetParent(this.transform.parent);
            LayoutElement _layoutElement = placeholder.AddComponent<LayoutElement>();
            _layoutElement.preferredWidth = this.GetComponent<LayoutElement>().preferredWidth;
            _layoutElement.preferredHeight = this.GetComponent<LayoutElement>().preferredHeight;

            placeholder.transform.SetSiblingIndex(this.transform.GetSiblingIndex());

            parentToReturnTo = this.transform.parent;
            placeholderParent = parentToReturnTo;
            this.transform.SetParent(this.transform.parent.parent);

            _canvasGroup.blocksRaycasts = false;
        }

        //On end drag card. Place card on finish
        public void OnEndDrag(PointerEventData eventData)
        {
            this.transform.SetParent(parentToReturnTo);
            this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
            _canvasGroup.blocksRaycasts = true;
            Destroy(placeholder);
        }
        #endregion Drag Functions
    }
}
