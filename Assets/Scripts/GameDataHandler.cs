﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace com.junglee.cards
{
    /// <summary>
    /// Handle game data
    /// </summary>
    public class GameDataHandler : MonoBehaviour
    {
        public Action OnWidgetAdded = delegate { };
        public Action ResetButtonState = delegate { };

        private const string _jsonPath = "GameData/GameData";

        public ReactiveProperty<bool> IsGameDataLoaded = new ReactiveProperty<bool>();

        [HideInInspector]
        public GameData m_CardsData;

        [HideInInspector]
        public List<GameObject> m_WidgetsList = new List<GameObject>();

        [HideInInspector]
        public List<GameObject> m_CurrentWidgets = new List<GameObject>();

        private void Start()
        {
            Application.targetFrameRate = 60;
        }

        //Create cards deck and After deck loaded
        public void OnCardsDeckLoaded()
        {
            GetGameData();
        }

        //Get json data ffom json
        private void GetGameData()
        {
            string jsonData = GetJsonStringFromPath(_jsonPath);
            if (!string.IsNullOrEmpty(jsonData))
            {
                HandleLevelDataLoad(jsonData);
            }
        }

        //parse data and creates data object
        private void HandleLevelDataLoad(string data)
        {
            m_CardsData = JsonUtility.FromJson<GameData>(data);

            Debug.Log("<<<<<<<<<<<<< Fetched game Data  >>>>>>>>>>>>>>>>>>>>>");

            IsGameDataLoaded.Value = true;
        }

        private string GetJsonStringFromPath(string path)
        {
            TextAsset textAsset = (TextAsset)Resources.Load(path);
            string jsonString = textAsset.text;

            Debug.Log("jsonString" + jsonString);

            return jsonString;
        }

        //Add clicked widget to list and hold them
        public void AddClickedWidget(GameObject widget)
        {
            m_WidgetsList.Add(widget);
            OnWidgetAdded();
        }
    }

    /// <summary>
    /// Data classes for hold data
    /// </summary>

    [Serializable]
    public class Value
    {
        public List<string> cards = new List<string>();
    }

    [Serializable]
    public class GameData
    {
        public int systime;
        public Value value = new Value();
        public string type;
    }

}