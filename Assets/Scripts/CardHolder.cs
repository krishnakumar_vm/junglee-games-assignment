﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace com.junglee.cards
{
    public class CardHolder : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        //on drop card to holder
        public void OnDrop(PointerEventData eventData)
        {
            CardWidget _widget = eventData.pointerDrag.GetComponent<CardWidget>();
            if (null != _widget)
                _widget.parentToReturnTo = this.transform;
        }

        //on poiter enter to holder
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (null != eventData.pointerDrag)
            {
                CardWidget _widget = eventData.pointerDrag.GetComponent<CardWidget>();
                if (null != _widget)
                    _widget.placeholderParent = this.transform;
            }
        }

        //on pointer exit from holder
        public void OnPointerExit(PointerEventData eventData)
        {
            if (null != eventData.pointerDrag)
            {
                CardWidget _widget = eventData.pointerDrag.GetComponent<CardWidget>();
                if (null != _widget && _widget.placeholderParent == this.transform)
                    _widget.placeholderParent = _widget.parentToReturnTo;
            }
        }
    }
}