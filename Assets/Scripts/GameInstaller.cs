﻿using Zenject;

namespace com.junglee.cards
{
    //installer for zenject
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        public GameDataHandler _GameDataHandler;
        public CardWidget _CardWidget;

        public override void InstallBindings()
        {
            Container.Bind<GameDataHandler>().FromComponentInNewPrefab(_GameDataHandler).AsSingle().NonLazy();

            Container.BindFactory<CardWidget, CardWidget.Factory>().FromComponentInNewPrefab(_CardWidget);
        }
    }
}