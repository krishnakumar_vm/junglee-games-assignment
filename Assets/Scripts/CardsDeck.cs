﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace com.junglee.cards
{
    /// <summary>
    /// Create All 52 cards and hold tham
    /// </summary>
    public class CardsDeck : MonoBehaviour
    {
        [Inject]
        GameDataHandler _GameDataHandler;
        [SerializeField]
        private List<Sprite> m_CardsDeck;

        [HideInInspector]
        public Dictionary<string, Sprite> m_CarsDict = new Dictionary<string, Sprite>();

        // s spade h hearts d diamonds c club
        private string[] _type = new string[] { "c", "d", "h", "s" };
        private string[] _value = new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13" };

        int cardIndex = 0;
        private void Start()
        {
            cardIndex = 0;
            for (int i = 0; i < _type.Length; i++)
            {
                for (int j = 0; j < _value.Length; j++)
                {
                    m_CarsDict.Add($"{ _value[j] + _type[i]}", m_CardsDeck[cardIndex]);
                    cardIndex++;
                }
            }

            //foreach (KeyValuePair<string, Sprite> kvp in carsDict)
            //{
            //    Debug.Log("Key = " + kvp.Key + "Value = ", kvp.Value);
            //}

            if (null != _GameDataHandler)
                _GameDataHandler.OnCardsDeckLoaded();
        }
    }
}
